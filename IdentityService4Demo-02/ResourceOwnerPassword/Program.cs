﻿using IdentityModel.Client;
using System;
using System.Net.Http;

namespace ResourceOwnerPassword
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            var client = new HttpClient();
            var disco = await client.GetDiscoveryDocumentAsync("https://localhost:44308");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return;
            }

            var tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address=disco.TokenEndpoint,
                ClientId="ClientId",
                ClientSecret="secret",

                UserName="Jesai",
                Password="123456",
                Scope="api1"
            });

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            Console.WriteLine(tokenResponse.Json);
            
        }
    }
}
